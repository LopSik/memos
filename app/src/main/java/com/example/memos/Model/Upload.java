package com.example.memos.Model;

import android.net.Uri;

public class Upload {

    private String mImageUrl;

    public Upload(){
        //
    }

    public Upload(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public String getmImageUrl() {
        return mImageUrl;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }
}
