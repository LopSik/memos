package com.example.memos;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.memos.Fragment.ProfileFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class RegistrationActivity extends AppCompatActivity {
    private EditText username, email, password;
    Button registr;

    FirebaseAuth auth;
    DatabaseReference reference;
    ProgressDialog pd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        registr = findViewById(R.id.registrat);

        auth = FirebaseAuth.getInstance();

        registr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd = new ProgressDialog(RegistrationActivity.this);
                pd.setMessage("Please wait...");
                pd.show();

                String str_username = username.getText().toString();
                str_username.replaceAll(" ","");
                String str_email = email.getText().toString();
                str_email.replaceAll(" ","");
                String str_password = password.getText().toString();
                str_password.replaceAll(" ","");

                if(TextUtils.isEmpty(str_username) || TextUtils.isEmpty(str_email)  || TextUtils.isEmpty(str_password)){
                    Toast.makeText(RegistrationActivity.this, "All fields are necessary to fill!",Toast.LENGTH_SHORT).show();
                }else if (str_password.length()<6){
                    Toast.makeText(RegistrationActivity.this, "Password must be at least 6 characters long!",Toast.LENGTH_SHORT).show();
                }else {
                    register(str_username,str_email,str_password);
                }
            }
        });
    }
    private void register(final String username, String email, String password){
        auth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            String userid = firebaseUser.getUid();

                            reference = FirebaseDatabase.getInstance().getReference().child("Users").child(userid);

                            HashMap<String, Object> hashMap = new HashMap<>();
                            hashMap.put("id", userid);
                            hashMap.put("username", username.toLowerCase());
                            hashMap.put("bio", "");
                            hashMap.put("imageurl", "https://firebasestorage.googleapis.com/v0/b/memos-eac32.appspot.com/o/toppng.com-file-svg-profile-icon-vector-980x980.png?alt=media&token=aee38843-bfdb-4c5e-9ce5-0adb8b994d99");
                            hashMap.put("status", "offline");

                            reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if (task.isSuccessful()){
                                        pd.dismiss();
                                        Intent intent = new Intent(RegistrationActivity.this, ProfileFragment.class );
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                }
                            });
                        }else {
                            pd.dismiss();
                            Toast.makeText(RegistrationActivity.this,"You cannot register for this login!",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
