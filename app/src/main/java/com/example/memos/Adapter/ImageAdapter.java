package com.example.memos.Adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.memos.EditImageActivity;
import com.example.memos.Model.Upload;
import com.example.memos.R;
import com.example.memos.ShablonsActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {

    private Context mContext;
    private List<String> mUploads;
    private ShablonInterface shablonInterface;


    public ImageAdapter(Context context, List<String> uploads, ShablonInterface shablonInterface){
        mContext = context;
        mUploads = uploads;
        this.shablonInterface = shablonInterface;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.mems_item, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
//        Upload uploadCurrent = mUploads.get(position);
        String uploadCurrent = mUploads.get(position);
        final Uri myUri = Uri.parse(uploadCurrent);
        Picasso.with(mContext)
                .load(uploadCurrent)
                .placeholder(R.drawable.cocosproz)
                .fit()
                .centerCrop()
                .into(holder.imageView);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shablonInterface.getShablon(myUri.toString());
            }
        });
    }


    @Override
    public int getItemCount() {
        return mUploads.size();
    }

    public class  ImageViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;

        public ImageViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image_view_upload);
        }
    }

    public interface ShablonInterface{
        void getShablon(String uri);
    }
}
