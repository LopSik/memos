package com.example.memos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.memos.Adapter.ImageAdapter;
import com.example.memos.Model.Upload;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class ShablonsActivity extends AppCompatActivity {
    private RecyclerView recyclerView;
    private ImageAdapter mAdapter;

    private ProgressBar mProgressCircle;

    private StorageReference reference;
    private List<String> mUploads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shablons);


        recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        mProgressCircle = findViewById(R.id.progress_circular);

        mUploads = new ArrayList<>();

        for (int i = 2; i <= 68 ; i++) {
            reference = FirebaseStorage.getInstance().getReference("memes").child("mem_" + i + ".jpg");

            reference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Log.d("Shablon", "onSuccess: "+ uri);
                    mUploads.add(uri.toString());
                    mProgressCircle.setVisibility(View.INVISIBLE);
                }
            });
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mAdapter = new ImageAdapter(ShablonsActivity.this, mUploads, new ImageAdapter.ShablonInterface() {
                    @Override
                    public void getShablon(String uri) {
                       sendShablon(uri);
                    }
                });
                recyclerView.setAdapter(mAdapter);
                mProgressCircle.setVisibility(View.INVISIBLE);
            }
        }, 5000);


    }

    private void sendShablon(String uri){
        Intent intent = new Intent(this, EditImageActivity.class);
        intent.putExtra("MyURI", uri);
        setResult(RESULT_OK, intent);
        finish();
    }
}
